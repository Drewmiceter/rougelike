﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

    public int hitPoints = 2;
    public Sprite damagedWallSprite;

    private SpriteRenderer spriteRenderer;

	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}
   
    public void DamageWall(int damageRecieved)
    {
        Debug.Log("Wall Damaged");
        hitPoints -= damageRecieved;
        spriteRenderer.sprite = damagedWallSprite;

        if(hitPoints <= 0)
        {
            Debug.Log("Wall Destroyed");
            gameObject.SetActive(false);
        }
    }
	
}
